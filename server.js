
const express = require('express');
var path = require('path');

// Constants
const PORT = process.env.PORT || 3000;

// App
const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, 'public')));


app.get('/', function(req, res, next) {
    res.render('index', { title: 'Bootcamp Institute' });
});

app.listen(PORT);
console.log('Running on http://localhost:' + PORT);

